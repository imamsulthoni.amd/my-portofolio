import React, { useEffect, useState } from "react";
import ponpesImage from "../images/ponpes-project.png";
import dataSantri from "../images/data-santri.png";
import ponpesProject from "../images/ponpes-project.png";
import formBRIVA from "../images/form-create-briva.png";
import listBRIVA from "../images/list-briva-santri.png";
import laporanBRIVA from "../images/laporan-pembayaran.png";
import dataSPP from "../images/data-pembayaran-spp.png";
import { SiLumen, SiNuxtDotJs, SiBootstrap, SiMysql } from "react-icons/si";
// import Carousel, { CarouselItem } from "../components/Carousel";

const Details = () => {
  const dataPreview = [
    {
      id: 1,
      title: "Data Entry Of Students",
      desc:
        "This app have 2 roles, admin and treasurer. admin can do data entry of students, when data of students created, the system will make BRIVA code for them automatically",
      image: dataSantri,
    },
    {
      id: 2,
      title: "BRIVA Menus",
      desc:
        "There are 6 menus that can treasurer use to craate BRIVA account for students, check the payment report, and see list of BRIVA account.",
      image: ponpesProject,
    },
    {
      id: 3,
      title: "Form To Create BRIVA Account",
      desc:
        "when the treasurer create BRIVA account of students, it will recorded in the bank of BRI. then, fee payment notes will be automatically made 12 for 12 months.",
      image: formBRIVA,
    },
    {
      id: 4,
      title: "List of BRIVA Accounts",
      desc:
        "in this menu, treasurer can see the list of BRIVA accounts, and also see which BRIVA Account has not been created.",
      image: listBRIVA,
    },
    {
      id: 5,
      title: "Fee Payment Report",
      desc:
        "treasurer can search for SPP payment reports on a certain date, then click the `Simpan` button to record the report, it will automatically change the fee payment status of student in a certain month to paid off.",
      image: laporanBRIVA,
    },
    {
      id: 6,
      title: "Fee Payment Records",
      desc:
        "on this menu the treasurer can see the tuition payment data from all students. In addition, the payment status can also be changed to paid off or not. but the payment status automatically becomes paid off when the treasurer presses the save button on the payment report menu.",
      image: dataSPP,
    },
  ];

  return (
    <div className="text-blueGray-700">
      <div className="container flex flex-col-reverse items-center px-5 py-16 mx-auto xl:flex-row md:px-12 lg:px-12">
        <div className="flex flex-col items-start text-left lg:flex-grow xl:w-1/2 xl:pr-24 md:pr-none md:mb-0">
          <h2 className="mb-2 mt-10 text-xs font-semibold tracking-widest text-black uppercase title-font">
            June 2021 - August 2021
          </h2>
          <h1 className="mb-8 text-2xl text-semibold tracking-tighter text-black md:text-4xl title-font">
            Fee Payment Management
          </h1>
          <h2 className="mb-4 text-xs font-bold tracking-widest uppercase text-black">
            Overview
          </h2>
          <p className="mb-8 text-sm md:text-base leading-relaxed text-gray-900 text-justify">
            Fee payment management web application using BRIVA payment method.
            This is a community service project belongs to the lecturer, which
            will be used to help the treasurer of Ponpes Dar Al Qur'an
            Arjawinangun in managing the payment of fees and allowance of
            students, and I created this app for my college final project. This
            application can be run a menu provided by the BRI API, such as
            creating a BRIVA account and view payment data of students in
            real-time. In addition, payment of fees of students for each month
            can be noted as well as be able to view the payment history of the
            allowance of students.
          </p>
          <h2 className="mb-4 text-xs font-bold tracking-widest uppercase text-black">
            Technology
          </h2>
          <ul className="grid grid-cols-4 gap-6 mb-8">
            <li className="flex flex-col justify-center items-center">
              <SiLumen className="w-8 h-8 text-red-500" />
              <p className="text-xs font-semibold text-gray-700 mt-2">Lumen</p>
            </li>
            <li className="flex flex-col justify-center items-center">
              <SiNuxtDotJs className="w-8 h-8 text-green-500" />
              <p className="text-xs font-semibold text-gray-700 mt-2">
                Nuxt.js
              </p>
            </li>
            <li className="flex flex-col justify-center items-center">
              <SiBootstrap className="w-8 h-8 text-purple-800" />
              <p className="text-xs font-semibold text-gray-700 mt-2">
                Bootstrap
              </p>
            </li>
            <li className="flex flex-col justify-center items-center">
              <SiMysql className="w-8 h-8 text-yellow-400" />
              <p className="text-xs font-semibold text-gray-700 mt-2">MySQL</p>
            </li>
          </ul>
        </div>
        <div className="w-full lg:w-5/6 lg:max-w-xl md:w-1/2">
          <img
            className="object-cover object-center rounded shadow-md"
            alt="hero"
            loading="lazy"
            src={ponpesImage}
          />
        </div>
      </div>
      <div className="px-5 py-16 mx-auto lg:px-28 bg-gray-50">
        <h2 className="mb-8 text-xl text-center font-bold tracking-widest uppercase text-black">
          Application Preview
        </h2>
        {dataPreview.map((data, index) => {
          return index % 2 == 0 ? (
            <div
              key={index}
              className="flex flex-col md:flex-row justify-around items-center py-8 md:py-20"
            >
              <div className="flex flex-col w-full md:w-1/3">
                <h1 className="mb-4 text-lg text-semibold tracking-tighter text-black md:mb-8 md:text-xl lg:text-3xl title-font">
                  {data.title}
                </h1>
                <p className="mb-2 text-sm">{data.desc}</p>
              </div>
              <div className="w-full  md:w-1/2">
                <img
                  className="object-cover object-center rounded shadow-md"
                  alt="hero"
                  loading="lazy"
                  src={data.image}
                />
              </div>
            </div>
          ) : (
            <div
              key={index}
              className="flex flex-col-reverse md:flex-row justify-around items-center py-8 md:py-20"
            >
              <div className="w-full  md:w-1/2">
                <img
                  className="object-cover object-center rounded shadow-md"
                  alt="hero"
                  loading="lazy"
                  src={data.image}
                />
              </div>
              <div className="flex flex-col w-full md:w-1/3">
                <h1 className="mb-4 text-lg text-semibold tracking-tighter text-black md:mb-8 md:text-xl lg:text-3xl title-font">
                  {data.title}
                </h1>
                <p className="mb-2 text-sm">{data.desc}</p>
              </div>
            </div>
          );
        })}
        {/* <Carousel>
          <CarouselItem>
            <img
              className="object-cover object-center rounded shadow-md"
              alt="hero"
              loading="lazy"
              src={ponpesImage}
            />
          </CarouselItem>
        </Carousel> */}
      </div>
    </div>
  );
};
export default Details;
