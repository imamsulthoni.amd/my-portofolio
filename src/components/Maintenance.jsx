import React from "react";
import MaintenanceIcon from "../images/icon-svg/Maintenance";
import { Link } from "react-router-dom";

const Maintenance = () => {
  return (
    <div className="py-16 xl:px-16 flex flex-col justify-center items-center">
      <MaintenanceIcon />
      <div className="flex flex-col justify-center items-center mb-6">
        <p className="text-md mt-6">I'm Sorry</p>
        <p className="text-md">This Page is Under Development</p>
      </div>
      <Link
        to="/"
        className="flex justify-center items-center text-sm px-4 py-1 bg-yellow-300 rounded-full"
      >
        <svg
          xmlns="http://www.w3.org/2000/svg"
          className="h-3 w-3 mr-3"
          viewBox="0 0 20 20"
          fill="currentColor"
        >
          <path
            fillRule="evenodd"
            d="M9.707 16.707a1 1 0 01-1.414 0l-6-6a1 1 0 010-1.414l6-6a1 1 0 011.414 1.414L5.414 9H17a1 1 0 110 2H5.414l4.293 4.293a1 1 0 010 1.414z"
            clipRule="evenodd"
          />
        </svg>
        Go back
      </Link>
    </div>
  );
};

export default Maintenance;
