module.exports = {
  purge: ["./index.html", "./src/**/*.{vue,js,ts,jsx,tsx}"],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      colors: {
        indigo: {
          // theme('colors.indigo.light')
          light: "#b3bcf5",

          // theme('colors.indigo.DEFAULT')
          DEFAULT: "#5c6ac4",
        },

        // theme('colors.indigo-dark')
        "react-color": "#64D9FA",
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
};
