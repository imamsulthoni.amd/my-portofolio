import { FaLaravel, FaVuejs, FaReact } from "react-icons/fa";
import {
  SiLumen,
  SiNuxtDotJs,
  SiBootstrap,
  SiTailwindcss,
} from "react-icons/si";

const Skills = () => {
  return (
    <div id="skills" className="h-auto xl:h-screen bg-white">
      <div className="py-16">
        <h1
          className="mb-8 text-2xl font-semibold tracking-tighter text-gray-800 lg:text-4xl title-font"
          data-aos="fade-up"
        >
          These Are What I Can Do
        </h1>
        <div className="my-20 xl:my-28 xl:px-28">
          <ul className="grid grid-cols-3 xl:grid-cols-5 gap-y-6 xl:gap-y-24">
            <li data-aos="fade-down" data-aos-duration="600" data-aos-delay="0">
              <FaLaravel className="h-16 w-16 md:h-28 md:w-28 text-gray-500 ability-icon hover:text-red-500" />
              <p className="text-sm md:text-md xl:text-lg font-semibold text-gray-700  mt-2">
                Laravel
              </p>
              {/*<p className="text-xs md:text-sm xl:text-md">68%</p>*/}
            </li>
            <li data-aos="fade-down" data-aos-duration="600" data-aos-delay="100">
              <SiLumen className="h-16 w-16 md:h-28 md:w-28  text-gray-500 hover:text-red-500 ability-icon" />
              <p className="text-sm md:text-md xl:text-lg font-semibold text-gray-700 mt-2">
                Lumen.
              </p>
              {/*<p className="text-xs md:text-sm xl:text-md">65%</p>*/}
            </li>
            {/* <li data-aos="fade-down" data-aos-duration="600" data-aos-delay="200">
              <FaVuejs className="h-16 w-16 md:h-28 md:w-28  text-gray-500 hover:text-green-700 ability-icon" />
              <p className="text-sm md:text-md xl:text-lg font-semibold text-gray-700 mt-2">
                Vue.js
              </p>
              <p className="text-xs md:text-sm xl:text-md">50%</p>
            </li> */}
            <li data-aos="fade-down" data-aos-duration="600" data-aos-delay="300">
              <SiNuxtDotJs className="h-16 w-16 md:h-28 md:w-28  text-gray-500 hover:text-green-700 ability-icon" />
              <p className="text-sm md:text-md xl:text-lg font-semibold text-gray-700 mt-2">
                Nuxt.js
              </p>
              {/*<p className="text-xs md:text-sm xl:text-md">50%</p>*/}
            </li>
            <li data-aos="fade-down" data-aos-duration="600" data-aos-delay="400">
              <FaReact className="h-16 w-16 md:h-28 md:w-28  text-gray-500 hover:text-react-color ability-icon" />
              <p className="text-sm md:text-md xl:text-lg font-semibold text-gray-700 mt-2">
                React.js
              </p>
              {/*<p className="text-xs md:text-sm xl:text-md">40%</p>*/}
            </li>
            <li data-aos="fade-down" data-aos-duration="600" data-aos-delay="500">
              <SiBootstrap className="h-16 w-16 md:h-28 md:w-28  text-gray-500 hover:text-purple-800 ability-icon" />
              <p className="text-sm md:text-md xl:text-lg font-semibold text-gray-700 mt-2">
                Bootstrap
              </p>
              {/*<p className="text-xs md:text-sm xl:text-md">70%</p>*/}
            </li>
            <li data-aos="fade-down" data-aos-duration="600" data-aos-delay="600">
              <SiTailwindcss className="h-16 w-16 md:h-28 md:w-28  text-gray-500 hover:text-blue-400 ability-icon" />
              <p className="text-sm md:text-md xl:text-lg font-semibold text-gray-700 mt-2">
                Tailwind CSS
              </p>
              {/*<p className="text-xs md:text-sm xl:text-md">45%</p>*/}
            </li>
          </ul>
        </div>
      </div>
    </div>
  );
};

export default Skills;
