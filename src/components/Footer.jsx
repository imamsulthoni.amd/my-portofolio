import Wave from "react-wavify";
const Footer = () => {
  return (
    <div id="footer" className="h-8 flex justify-center items-center">
      <Wave
        className="absolute"
        fill="#140a42"
        paused={false}
        options={{
          height: 20,
          amplitude: 20,
          speed: 0.25,
          points: 3,
        }}
      />
      <div className="flex flex-col justify-center items-center px-3">
        <p className="text-center text-gray-200 z-10 text-xs md:text-md font-light font-mono mt-10">
          "The scariest moment is always just before you start."
        </p>
        <p className="text-gray-400 z-10 text-xs md:text-md font-light italic">
          - Stephen King
        </p>
      </div>
    </div>
  );
};

export default Footer;
