import { useState, useEffect } from "react";
import { NavHashLink as Link } from "react-router-hash-link";

const ToTopButton = () => {
  const scrollWithOffset = (el, offset) => {
    window.scroll({
      top: el.offsetTop - offset,
      left: 0,
      behavior: "smooth",
    });
  };

  const [showButton, setShowButton] = useState(false);

  useEffect(() => {
    window.addEventListener("scroll", () => {
      if (window.pageYOffset > 600) {
        setShowButton(true);
      } else {
        setShowButton(false);
      }
    });
  }, []);

  return (
    <div className="absolute">
      <Link to="/#top" scroll={(el) => scrollWithOffset(el, 30)} exact>
        <svg
          xmlns="http://www.w3.org/2000/svg"
          className={`h-10 w-10 z-30 fixed transition duration-500 opacity-0 bottom-10 right-6 bg-yellow-400 hover:bg-yellow-500 p-3 rounded-full ${
            showButton && `transition duration-500 opacity-100`
          }`}
          fill="none"
          viewBox="0 0 24 24"
          stroke="currentColor"
        >
          <path
            strokeLinecap="round"
            strokeLinejoin="round"
            strokeWidth={2}
            d="M5 11l7-7 7 7M5 19l7-7 7 7"
          />
        </svg>
      </Link>
    </div>
  );
};

export default ToTopButton;
