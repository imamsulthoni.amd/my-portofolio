import { NavHashLink as Link } from "react-router-hash-link";
import {
  AiOutlineInstagram,
  AiOutlineGitlab,
  AiFillLinkedin,
} from "react-icons/ai";
import { FaTelegramPlane } from "react-icons/fa";

const Navbar = ({ navToggle, isOpen }) => {
  const scrollWithOffset = (el, offset) => {
    window.scroll({
      top: el.offsetTop - offset,
      left: 0,
      behavior: "smooth",
    });
  };

  return (
    <nav
      className={`sticky top-0 h-16 flex justify-between items-center bg-gray-50 md:bg-white shadow-sm ${
        isOpen && `shadow-none`
      } z-10`}
    >
      <div className="flex space-x-4 pl-2 pr-4 py-3 rounded-r-3xl text-2xl bg-yellow-300">
        <p className="hidden lg:block text-sm">Find me on</p>
        <a href="https://t.me/imamsulthoni">
          <FaTelegramPlane className="transform transition duration-500 hover:scale-110" />
        </a>
        <a href="https://www.instagram.com/rnzn_hfogslmr/">
          <AiOutlineInstagram className="transform transition duration-500 hover:scale-110" />
        </a>
        <a href="https://gitlab.com/imamsulthoni.amd">
          <AiOutlineGitlab className="transform transition duration-500 hover:scale-110" />
        </a>
        <a href="https://www.linkedin.com/in/imam-sulthoni-2b44161a3/">
          <AiFillLinkedin className="transform transition duration-500 hover:scale-110" />
        </a>
      </div>
      <div className="px-4 cursor-pointer md:hidden">
        {isOpen ? (
          <svg
            xmlns="http://www.w3.org/2000/svg"
            className="h-6 w-6"
            fill="none"
            viewBox="0 0 24 24"
            stroke="currentColor"
            onClick={navToggle}
          >
            <path
              strokeLinecap="round"
              strokeLinejoin="round"
              strokeWidth={2}
              d="M6 18L18 6M6 6l12 12"
            />
          </svg>
        ) : (
          <svg
            xmlns="http://www.w3.org/2000/svg"
            className="h-6 w-6"
            fill="none"
            viewBox="0 0 24 24"
            stroke="currentColor"
            onClick={navToggle}
          >
            <path
              strokeLinecap="round"
              strokeLinejoin="round"
              strokeWidth={2}
              d="M4 6h16M4 12h16m-7 6h7"
            />
          </svg>
        )}
      </div>
      <div className="pr-8 md:block hidden">
        <Link
          to={"/#top"}
          className="mr-8 p-2 text-gray-700"
          activeClassName="transform transition duration-350 border-b-2 border-yellow-500"
          scroll={(el) => scrollWithOffset(el, 0)}
          exact
        >
          Home
        </Link>
        <Link
          to={"/#about"}
          className="mr-8 p-2 text-gray-700"
          activeClassName="transform transition duration-350 border-b-2 border-yellow-500"
          scroll={(el) => scrollWithOffset(el, 30)}
          exact
        >
          About
        </Link>
        <Link
          to="/#project"
          className="mr-8 p-2 text-gray-700"
          activeClassName="transform transition duration-350 border-b-2 border-yellow-500"
          scroll={(el) => scrollWithOffset(el, 30)}
          exact
        >
          Project
        </Link>
        <Link
          to={"/#skills"}
          className="mr-8 p-2 text-gray-700"
          activeClassName="transform transition duration-350 border-b-2 border-yellow-500"
          scroll={(el) => scrollWithOffset(el, 30)}
          exact
        >
          Skills
        </Link>
        <Link
          to={"/#contact"}
          className="mr-8 p-2 text-gray-700"
          activeClassName="transform transition duration-350 border-b-2 border-yellow-500"
          scroll={(el) => scrollWithOffset(el, 45)}
          exact
        >
          Contact
        </Link>
      </div>
    </nav>
  );
};

export default Navbar;
