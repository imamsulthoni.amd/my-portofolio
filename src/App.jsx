import "./App.css";
import { useState, useEffect } from "react";
import About from "./pages/About";
import Hero from "./pages/Hero";
import Navbar from "./components/Navbar";
import Project from "./pages/Project";
import Footer from "./components/Footer";
import Skills from "./pages/Skills";
import Contact from "./pages/Contact";
import ToTopButton from "./components/ToTopButton";
import NavDropdown from "./components/NavDropdown";
import { HashRouter as Router, Link, Route, Switch } from "react-router-dom";
import AOS from "aos";
import "aos/dist/aos.css";
import Details from "./pages/Details";

function MainPage(props) {
  return (
    <div className="App">
      <ToTopButton />
      <Hero />
      <About />
      <Project />
      <Skills />
      <Contact />
      <Footer />
    </div>
  );
}

function App() {
  const [navToggle, setnavToggle] = useState(false);

  const openNavDropdown = () => {
    setnavToggle(!navToggle);
  };

  useEffect(() => {
    AOS.init();
    AOS.refresh();
  }, []);

  return (
    <Router basename="/">
      <Navbar navToggle={openNavDropdown} isOpen={navToggle} />
      {navToggle && <NavDropdown />}
      <Switch>
        <Route exact path="/project/details" component={Details} />
        <Route exact path="/" component={MainPage} />
      </Switch>
    </Router>
  );
}

export default App;
