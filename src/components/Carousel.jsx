import React, { useState } from "react";
import { useSwipeable } from "react-swipeable";
import { AiOutlineLeft } from "react-icons/ai";

export const CarouselItem = ({ children, width }) => {
  return (
    <div className="inline-flex relative" style={{ width: width }}>
      <button className="absolute bg-yellow-100 rounded-full p-2 hover:bg-yellow-300">
        <AiOutlineLeft className="text-2xl" />
      </button>
      {children}
    </div>
  );
};

const Carousel = ({ children }) => {
  const [activeIndex, setActiveIndex] = useState(0);

  const updateIndex = (newIndex) => {
    if (newIndex < 0) {
      newIndex = React.Children.count(children) - 1;
    } else if (newIndex >= React.Children.count(children)) {
      newIndex = 0;
    }

    setActiveIndex(newIndex);
  };

  const handlers = useSwipeable({
    onSwipedLeft: () => updateIndex(activeIndex + 1),
    onSwipedRight: () => updateIndex(activeIndex - 1),
  });

  return (
    <div {...handlers} className="overflow-hidden w-full md:w-4/5 mx-auto">
      <div
        className="whitespace-nowrap transition delay-3000"
        style={{ transform: `translateX(-${activeIndex * 100}%)` }}
      >
        {React.Children.map(children, (child, index) => {
          return React.cloneElement(child, { width: "100%" });
        })}
      </div>
      <div className="flex space-x-5 justify-center mt-4">
        <button
          className="bg-yellow-300 py-2 px-5"
          onClick={() => {
            updateIndex(activeIndex - 1);
          }}
        >
          Prev
        </button>
        <button
          className="bg-blue-300 py-2 px-5"
          onClick={() => {
            updateIndex(activeIndex + 1);
          }}
        >
          Next
        </button>
      </div>
    </div>
  );
};

export default Carousel;
