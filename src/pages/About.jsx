import imamImage from "../images/imam.png";

const About = () => {
  return (
    <div id="about" className="h-auto xl:h-auto bg-gray-50">
      <div className="py-16 xl:px-32">
        <h1
          className="mb-8 text-2xl font-semibold tracking-tighter text-gray-800 lg:text-4xl title-font"
          data-aos="fade-up"
          data-aos-delay="100"
        >
          About Me
        </h1>
        <div
          className="my-20 space-y-12 lg:space-y-0 lg:mb-20 md:px-24 lg:px-32 xl:px-64"
          data-aos="fade-up"
          data-aos-delay="200"
          data-aos-easing="ease-in-out"
          data-aos-duration="500"
        >
          <div className="group relative">
            <div className="absolute inset-0 bg-gradient-to-r bg-green-200 shadow-md transform -skew-y-6 sm:skew-y-0 sm:-rotate-6 sm:rounded-3xl"></div>
            <div className="absolute inset-0 bg-gradient-to-r bg-yellow-300 shadow-md transform -skew-y-6 sm:skew-y-0 sm:-rotate-2 sm:rounded-3xl"></div>
            <figure className="bg-white rounded-xl p-8 md:p-2 relative shadow-md transfom -skew-y-6">
              <img
                className="w-40 h-40 rounded-full mx-auto object-cover object-center border-double border-4 bg-yellow-500 mt-6"
                src={imamImage}
                alt=""
              ></img>
              <div className="pt-6 md:p-8 text-justify space-y-4">
                <blockquote>
                  <p className="text-md text-gray-800">
                    I was born in Bumi Dipasena Agung, Lampung, Indonesia. Since
                    middle school I settled in West Java. I get the
                    title of the Diploma III of Informatics from Politeknik Negeri Indramayu in
                    2021. From November 2020 to March 2021, I joined PT Bejana
                    Investidata Globalindo in the internship program.
                  </p>
                  <p className="text-md text-gray-800">
                    I love to learn new things because it can open up new
                    insights about the development of technology, and also I
                    interested in the manufacture or development of
                    application-based website, and lately I have made a web
                    application for manage fee payment of student using the framework laravel lumen
                    and Nuxt js, and also BRI API for integration with BRI services.
                  </p>
                </blockquote>
              </div>
            </figure>
          </div>
        </div>
      </div>
    </div>
  );
};

export default About;
