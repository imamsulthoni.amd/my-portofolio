import { NavHashLink as Link } from "react-router-hash-link";

const Hero = () => {
  const scrollWithOffset = (el, offset) => {
    window.scroll({
      top: el.offsetTop - offset,
      left: 0,
      behavior: "smooth",
    });
  };

  return (
    <div
      id="hero"
      className="bg-white h-screen flex flex-col justify-center items-center"
    >
      <div className="px-2">
        <h1 className="text-3xl sm:text-4xl md:text-6xl mb-10">
          Hi, I'm Imam Sulthoni
        </h1>
      </div>
      <div className="px-2 grid grid-cols-6">
        <div className="col-span-6 md:col-start-2 md:col-span-4 xl:col-start-3 xl:col-span-2">
          <p className="mb-32 text-md text-gray-700">
            I am happy to explore something new about web programming and are
            interested in the manufacture or development of application-based
            website.
          </p>
        </div>
      </div>
      <Link
        className="flex items-center text-sm py-4 md:py-5 px-10 md:text-lg bg-yellow-300 rounded-full hover:bg-yellow-500 transition duration-100 ease-in-out animate-bounce"
        to={"/#about"}
        scroll={(el) => scrollWithOffset(el, 0)}
        exact
      >
        See More
        <svg
          xmlns="http://www.w3.org/2000/svg"
          className="h-4 md:h-6 w-6 ml-4"
          fill="none"
          viewBox="0 0 24 24"
          stroke="currentColor"
        >
          <path
            strokeLinecap="round"
            strokeLinejoin="round"
            strokeWidth={2}
            d="M19 14l-7 7m0 0l-7-7m7 7V3"
          />
        </svg>
      </Link>
    </div>
  );
};

export default Hero;
