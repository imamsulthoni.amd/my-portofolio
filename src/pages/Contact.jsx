import React from "react";
import { useForm } from "react-hook-form";
import Mailbox from "../images/icon-svg/Mailbox";

const Contact = () => {
  const {
    register,
    handleSubmit,
    formState: { errors },
    reset,
  } = useForm();

  const onSubmit = (data, r) => {
    alert(`Thank you ${data.name} for your message`);
    const templateId = "template_hz8zkhc";
    const serviceID = "service_emailjs";
    sendFeedback(serviceID, templateId, {
      from_name: data.name,
      message: data.comment,
      reply_to: data.email,
    });
    r.target.reset();
    reset("", {
      keepValues: false,
    });
  };

  const sendFeedback = (serviceID, templateId, variables) => {
    window.emailjs
      .send(serviceID, templateId, variables)
      .then((res) => {
        console.log("Email successfully sent!");
      })
      .catch((err) =>
        console.error(
          "There has been an error.  Here some thoughts on the error that occured:",
          err
        )
      );
  };

  return (
    <div
      id="contact"
      className="h-auto xl:h-screen bg-gray-50 justify-center py-16 lg:flex lg:space-x-32 lg:px-20"
    >
      <div
        className="text-center lg:text-left flex flex-col"
        data-aos="zoom-in"
        data-aos-duration="600"
      >
        <h2 className="text-4xl lg:text-5xl font-bold leading-tight">
          Leave me a
        </h2>
        <h2 className="text-4xl lg:text-5xl font-bold leading-tight">
          message
        </h2>
        <div className="hidden lg:block">
          <Mailbox />
        </div>
      </div>
      <form
        data-aos="zoom-in"
        data-aos-delay="100"
        data-aos-duration="600"
        id="contact-form"
        className="px-6 py-16 lg:py-0 md:px-20 lg:px-0 w-full xl:max-w-lg justify-center"
        onSubmit={handleSubmit(onSubmit)}
      >
        <div className="flex flex-wrap -mx-3 mb-6">
          <div className="w-full px-3">
            <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2 text-left">
              Full Name
            </label>
            <input
              className="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
              id="name"
              type="text"
              name="name"
              {...register("name", { required: true })}
            />
            <p className="text-red-600 text-xs italic">
              {errors.name?.type === "required" && "your name must be filled"}
            </p>
          </div>
        </div>
        <div className="flex flex-wrap -mx-3 mb-6">
          <div className="w-full px-3">
            <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2 text-left">
              E-mail
            </label>
            <input
              className="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
              id="email"
              type="email"
              name="email"
              {...register("email", { required: true })}
            />
            <p className="text-red-600 text-xs italic">
              {errors.email?.type === "required" && "your email must be filled"}
            </p>
          </div>
        </div>
        <div className="flex flex-wrap -mx-3 mb-6">
          <div className="w-full px-3">
            <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2 text-left">
              Message
            </label>
            <textarea
              className=" no-resize appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white focus:border-gray-500 h-48 resize-none"
              id="message"
              name="comment"
              {...register("comment", { required: true })}
            ></textarea>
            <p className="text-red-600 text-xs italic">
              {errors.comment?.type === "required" && "message must be filled"}
            </p>
          </div>
        </div>
        <div className="md:flex md:justify-center md:items-center">
          <div className="md:w-1/3">
            <button
              className="shadow bg-yellow-300 hover:bg-yellow-400 focus:shadow-outline focus:outline-none text-dark font-semibold py-2 px-8 rounded"
              type="submit"
            >
              Send
            </button>
          </div>
        </div>
      </form>
    </div>
  );
};

export default Contact;
