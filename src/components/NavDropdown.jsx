import { HashLink as Link } from "react-router-hash-link";

const NavDropdown = () => {
  const scrollWithOffset = (el, offset) => {
    window.scroll({
      top: el.offsetTop - offset,
      left: 0,
      behavior: "smooth",
    });
  };
  return (
    <div className="sticky top-16 z-10 flex flex-col bg-gray-50 shadow-md items-center md:hidden mx-2 rounded-b-lg">
      <Link
        to={"/#top"}
        className="p-2 text-gray-700"
        activeclassname="transform transition duration-350 border-b-2 border-blue-500"
        scroll={(el) => scrollWithOffset(el, 0)}
      >
        Home
      </Link>
      <Link
        to={"/#about"}
        className="p-2 text-gray-700"
        activeclassname="transform transition duration-350 border-b-2 border-yellow-500"
        scroll={(el) => scrollWithOffset(el, 30)}
      >
        About
      </Link>
      <Link
        to="/#project"
        className="p-2 text-gray-700"
        activeclassname="transform transition duration-350 border-b-2 border-yellow-500"
        scroll={(el) => scrollWithOffset(el, 30)}
      >
        project
      </Link>
      <Link
        to={"/#skills"}
        className="p-2 text-gray-700"
        activeclassname="transform transition duration-350 border-b-2 border-yellow-500"
        scroll={(el) => scrollWithOffset(el, 30)}
      >
        Skills
      </Link>
      <Link
        to={"/#contact"}
        className="p-2 text-gray-700"
        activeclassname="transform transition duration-350 border-b-2 border-yellow-500"
        scroll={(el) => scrollWithOffset(el, 45)}
      >
        Contact
      </Link>
    </div>
  );
};

export default NavDropdown;
