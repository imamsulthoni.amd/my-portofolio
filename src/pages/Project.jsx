import ponpesImage from "../images/ponpes-project.png";
import { Link } from "react-router-dom";

const Project = () => {
  return (
    <div id="project" className="h-auto xl:h-screen bg-green-200">
      <div className="py-16 xl:px-16">
        <h1
          className="mb-8 text-2xl font-semibold tracking-tighter text-gray-800 lg:text-4xl title-font"
          data-aos="fade-up"
          data-aos-delay="100"
        >
          My Recent Project
        </h1>
        <div className="container flex flex-col px-5 py-16 mx-auto lg:items-center md:flex-row lg:px-28">
          <div className="w-full mb-10 lg:w-5/6 lg:max-w-sm xl:max-w-lg md:w-1/2 transform transition duration-500 hover:scale-105">
            <img
              data-aos="fade-down"
              data-aos-easing="ease-in-out"
              data-aos-duration="600"
              className="object-cover object-center rounded shadow-md"
              alt="hero"
              loading="lazy"
              src={ponpesImage}
            />
          </div>
          <div
            className="flex flex-col items-start text-left lg:flex-grow md:w-1/2 lg:pl-24 md:pl-16"
            data-aos="fade-up"
            data-aos-delay="200"
            data-aos-easing="ease-in-out"
            data-aos-duration="600"
          >
            <h1 className="mb-2 text-2xl text-semibold tracking-tighter text-left text-gray xl:text-3xl title-font">
              Fee Payment Management
            </h1>
            <p className="mb-6 text-sm text-red-500 text-left lg:text-center">
              June 2021 - August 2021
            </p>
            <p className="mb-6 text-base leading-relaxed text-justify text-blueGray-700 ">
              Fee payment management web application using BRIVA payment method.
              This is a community service project belongs to the lecturer, which
              will be used to help the treasurer of Ponpes Dar Al Qur'an
              Arjawinangun in managing the payment of fees and allowance of
              students.
            </p>
            <Link
              to="/project/details"
              className="flex items-center py-2 px-4 text-xs bg-yellow-300 hover:bg-yellow-500 rounded-full"
            >
              See the details
              <svg
                xmlns="http://www.w3.org/2000/svg"
                className="h-5 w-5 ml-3"
                viewBox="0 0 20 20"
                fill="currentColor"
              >
                <path
                  fillRule="evenodd"
                  d="M10.293 5.293a1 1 0 011.414 0l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414-1.414L12.586 11H5a1 1 0 110-2h7.586l-2.293-2.293a1 1 0 010-1.414z"
                  clipRule="evenodd"
                />
              </svg>
            </Link>
          </div>
        </div>
        {/* <div className="px-4 space-y-10 md:px-16 lg:flex lg:space-y-0 lg:space-x-24 lg:px-26 xl:px-32">
          <div className="relative">
            <div className="w-full shadow-md hover:shadow-none cursor-pointer">
              <img
                src={ponpesImage}
                alt="bigio-image"
                className="w-full h-full object-center object-cover"
              />
            </div>
            <div className="px-2">
              <div className="flex justify-between items-center lg:flex-col lg:justify-center">
                <h1 className="text-md font-semibold text-left mt-2">
                  Fee Payment Web App
                </h1>
              </div>
              <h1 className="text-sm text-left lg:text-center">
                Fee payment management web application using BRIVA payment
                method
              </h1>
              <p className="text-sm text-red-500 text-left lg:text-center">
                Laravel Lumen / Nuxt.js / BRI API
              </p>
            </div>
          </div>
        </div> */}
      </div>
    </div>
  );
};

export default Project;
